package com.example.monapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MenuActivity extends AppCompatActivity {

    private Button bProposeOffre;
    private Button bConsulteOffre;
    private Button bdeconnecter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        bProposeOffre = (Button)findViewById(R.id.bAddOffre);
        bConsulteOffre = (Button)findViewById(R.id.bConsulter_Offres);
        bdeconnecter = (Button) findViewById(R.id.bsedeconnecter);


        //Connect Button
        bProposeOffre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClassName(getApplicationContext(), "com.example.monapplication.FormeOffreActivity");
                startActivity(i);


            }
        });

        bConsulteOffre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent();
                i.setClassName(getApplicationContext(), "com.example.monapplication.MapsActivity");
                startActivity(i);

            }
        });

        bdeconnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });



    }
}