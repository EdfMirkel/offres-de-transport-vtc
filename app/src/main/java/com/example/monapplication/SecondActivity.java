package com.example.monapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SecondActivity extends AppCompatActivity {

    DatabaseHelper db = new DatabaseHelper(this);
    private EditText nom, prenom, tel, email, login, password, repassword;
    private Button bvalider;
    private Button bannuler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        nom = (EditText)findViewById(R.id.editTextAddAcountPersonLastName);
        prenom = (EditText)findViewById(R.id.editTextAddAcountPersonFirstName);
        tel = (EditText)findViewById(R.id.editTextAddAcountPhone);
        email = (EditText)findViewById(R.id.editTextAddAcountEmailAddress);
        login = (EditText) findViewById(R.id.editTextAddAcountPersonLogin);
        password = (EditText) findViewById(R.id.editTextAddAcountPassword);
        repassword = (EditText) findViewById(R.id.editTextAddAcountPasswordConfirmations);
        bvalider = (Button)findViewById(R.id.buttonAddAcount_valider);
        bannuler = (Button)findViewById(R.id.buttonAddAcount_annuler);

        bvalider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Nom = nom.getText().toString();
                String Prenom = prenom.getText().toString();
                String Tel = tel.getText().toString();
                String Email = email.getText().toString();
                String Login = login.getText().toString();
                String Password = password.getText().toString();
                String Repassword = repassword.getText().toString();

                if(Nom.equals("")|| Prenom.equals("")|| Tel.equals("") || Email.equals("") || Login.equals("") || Password.equals("") || Repassword.equals("")){
                    Toast.makeText(getApplicationContext(), "Veuillez remplir tous les champs, svp !", Toast.LENGTH_SHORT).show();
                }else {
                    if(Password.equals(Repassword)){
                        Boolean CheckLogin = db.ChekLogin(Login);

                        if(CheckLogin == false){

                            boolean result = db.addData(Nom, Prenom, Tel, Email , Login  ,  Password);
                            if( result == true ){
                                Toast.makeText(getApplicationContext(), "successfully registered !", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent();
                                i.setClassName(getApplicationContext(), "com.example.monapplication.MainActivity");
                                i.putExtra("Login", Login);
                                i.putExtra("Password", Password);
                                startActivity(i);
                            }else {
                                Toast.makeText(getApplicationContext(), "ERREUR !", Toast.LENGTH_SHORT).show();
                            }

                        }else {
                            Toast.makeText(getApplicationContext(), "Login déja utiliser !", Toast.LENGTH_SHORT).show();
                        }

                    }else {
                        Toast.makeText(getApplicationContext(), "mot de passe de confirmation est incorrecte  !", Toast.LENGTH_SHORT).show();

                    }

                }
            }

        });


        bannuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}