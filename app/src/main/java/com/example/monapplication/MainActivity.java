package com.example.monapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private Button bConnecter;
    private Button bAddAccount;
    DatabaseHelper db = new DatabaseHelper(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.editTextTextPersonName);
        password = (EditText) findViewById(R.id.editTextTextPassword);
        bConnecter = (Button)findViewById(R.id.button);
        bAddAccount = (Button)findViewById(R.id.button2);

        //Create Account Button
        bAddAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClassName(getApplicationContext(), "com.example.monapplication.SecondActivity");
                i.putExtra("monmessage","message");
                startActivity(i);
            }
        });


//Connect Button
        bConnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Username = username.getText().toString();
                String Password = password.getText().toString();

                if(!Username.equals("")|| !Password.equals("")){
                    boolean check = db.checkconnection(Username, Password);
                    if(check == true){
                        Toast.makeText(getApplicationContext(), "Login Réussite !", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent();
                        i.setClassName(getApplicationContext(), "com.example.monapplication.MenuActivity");
                        startActivity(i);
                    }else
                        Toast.makeText(getApplicationContext(), "ce login n'existe pas!", Toast.LENGTH_SHORT).show();
                }

            }
        });




    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String userperson = getIntent().getStringExtra("Login");
        String passperson = getIntent().getStringExtra("Password");
        username.setText(userperson);
        password.setText(passperson);
    }
}