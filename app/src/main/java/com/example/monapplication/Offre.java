package com.example.monapplication;

public class Offre {

    private int ID;
    private String NOM ;
    private String PRENOM ;
    private String  MODELE ;
    private String COULEUR ;
    private String IMMATRICULATION ;
    private String LIEU_DU_DRV;
    private String DESTINATION ;
    private String HEUR_DU_RDV ;
    private String NOMBRE_DE_PLACES_PROPOSEES ;

    public Offre() {

    }

    public Offre(int ID ,String NOM, String PRENOM, String MODELE, String COULEUR, String IMMATRICULATION, String LIEU_DU_DRV, String DESTINATION, String HEUR_DU_RDV, String NOMBRE_DE_PLACES_PROPOSEES) {
        this.ID = ID;
        this.NOM = NOM;
        this.PRENOM = PRENOM;
        this.MODELE = MODELE;
        this.COULEUR = COULEUR;
        this.IMMATRICULATION = IMMATRICULATION;
        this.LIEU_DU_DRV = LIEU_DU_DRV;
        this.DESTINATION = DESTINATION;
        this.HEUR_DU_RDV = HEUR_DU_RDV;
        this.NOMBRE_DE_PLACES_PROPOSEES = NOMBRE_DE_PLACES_PROPOSEES;
    }
    public int getID() {
        return ID;
    }


    public String getNOM() {
        return NOM;
    }

    public String getPRENOM() {
        return PRENOM;
    }

    public String getMODELE() {
        return MODELE;
    }

    public String getCOULEUR() {
        return COULEUR;
    }

    public String getIMMATRICULATION() {
        return IMMATRICULATION;
    }

    public String getLIEU_DU_DRV() {
        return LIEU_DU_DRV;
    }

    public String getDESTINATION() {
        return DESTINATION;
    }

    public String getHEUR_DU_RDV() {
        return HEUR_DU_RDV;
    }

    public String getNOMBRE_DE_PLACES_PROPOSEES() {
        return NOMBRE_DE_PLACES_PROPOSEES;
    }

    public void setNOM(String NOM) {
        this.NOM = NOM;
    }

    public void setPRENOM(String PRENOM) {
        this.PRENOM = PRENOM;
    }

    public void setMODELE(String MODELE) {
        this.MODELE = MODELE;
    }

    public void setCOULEUR(String COULEUR) {
        this.COULEUR = COULEUR;
    }

    public void setIMMATRICULATION(String IMMATRICULATION) {
        this.IMMATRICULATION = IMMATRICULATION;
    }

    public void setLIEU_DU_DRV(String LIEU_DU_DRV) {
        this.LIEU_DU_DRV = LIEU_DU_DRV;
    }

    public void setDESTINATION(String DESTINATION) {
        this.DESTINATION = DESTINATION;
    }

    public void setHEUR_DU_RDV(String HEUR_DU_RDV) {
        this.HEUR_DU_RDV = HEUR_DU_RDV;
    }

    public void setNOMBRE_DE_PLACES_PROPOSEES(String NOMBRE_DE_PLACES_PROPOSEES) {
        this.NOMBRE_DE_PLACES_PROPOSEES = NOMBRE_DE_PLACES_PROPOSEES;
    }

    public void setID(int parseInt) {
    }
}
