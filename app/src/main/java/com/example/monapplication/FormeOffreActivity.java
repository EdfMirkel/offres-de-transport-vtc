package com.example.monapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FormeOffreActivity extends AppCompatActivity {

    DatabaseHelper db = new DatabaseHelper(this);
    private  Button bAddOffre;
    private  EditText nom;
    private EditText prenom;
    private EditText modele;
    private EditText couleur;
    private EditText immatriculion;
    private EditText lieu_du_rdv;
    private EditText destination;
    private EditText heur_du_rdv;
    private EditText nombre_de_places_proposees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forme_offre);

        bAddOffre =(Button) findViewById(R.id.bValiderOffre);
        nom = (EditText) findViewById(R.id.editTextOffrePersonName);
        prenom = (EditText) findViewById(R.id.editTextOffrePersonFIRST_NAME);
        modele = (EditText) findViewById(R.id.editTextOffreMODELE);
        couleur = (EditText) findViewById(R.id.editTextOffreCOULEUR);
        immatriculion = (EditText) findViewById(R.id.editTextOffreIMMATRICULATION);
        lieu_du_rdv = (EditText) findViewById(R.id.editTextOffreLIEU_DU_DRV);
        destination = (EditText) findViewById(R.id.editTextOffreDESTINATION);
        heur_du_rdv = (EditText) findViewById(R.id.editTextOffreHEUR_DU_RDV);
        nombre_de_places_proposees = (EditText) findViewById(R.id.editTextOffreNOMBRE_DE_PLACES_PROPOSEES);


        bAddOffre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Nom = nom.getText().toString();
                String Prenom = prenom.getText().toString();
                String Modele = modele.getText().toString();
                String Couleur = couleur.getText().toString();
                String Immatriculion = immatriculion.getText().toString();
                String Lieu_du_rdv = lieu_du_rdv.getText().toString();
                String Destination = destination.getText().toString();
                String Heur_du_rdv = heur_du_rdv.getText().toString();
                String Nombre_de_places_proposees = nombre_de_places_proposees.getText().toString();


                if(Nom.equals("")|| Prenom.equals("")|| Modele.equals("") || Couleur.equals("") || Immatriculion.equals("") || Lieu_du_rdv.equals("") ||Destination.equals("") || Heur_du_rdv.equals("") || Nombre_de_places_proposees.equals("")){
                    Toast.makeText(getApplicationContext(), "Veuillez remplir tous les champs, svp !", Toast.LENGTH_SHORT).show();
                }else {

                }
                boolean result ;
                result = db.addData(Nom, Prenom, Modele, Couleur, Immatriculion, Lieu_du_rdv, Destination, Heur_du_rdv, Nombre_de_places_proposees);
                if( result == true ){
                    Toast.makeText(getApplicationContext(), "successfully registered !", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(), "ERREUR !", Toast.LENGTH_SHORT).show();
                }
            }
        });






    }
}