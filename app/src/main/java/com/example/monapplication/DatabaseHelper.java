package com.example.monapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 2/28/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    private static final String TABLE_NAME = "offre_table";
    private static final String TABLE_NAME_Person = "person";


    // TABLE  OFFRE COLLUMN
    private static final String ID = "id";
    private static final String NOM = "nom";
    private static final String PRENOM = "prenom";
    private static final String MODELE = "MODELE";
    private static final String COULEUR = "couleur";
    private static final String IMMATRICULATION = "immatriculion";
    private static final String LIEU_DU_DRV = "lieu_du_rdv";
    private static final String DESTINATION = "destination";
    private static final String HEUR_DU_RDV = "heur_du_rdv";
    private static final String NOMBRE_DE_PLACES_PROPOSEES = "nombre_de_place_proposer";




    // TABLE  PERSON COLLUMN
    //private static final String ID = "id";
    //private static final String NOM = "nom";
   // private static final String PRENOM = "prenom";
    private static final String TEL = "tel";
    private static final String EMAIL = "email";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";



    public DatabaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // CREATE TABLE OFFRE
        String createTable = "CREATE TABLE " + TABLE_NAME + " ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NOM + " TEXT, " +
                PRENOM + " TEXT, " +
                MODELE + " TEXT, " +
                COULEUR + " TEXT, " +
                IMMATRICULATION + " TEXT, " +
                LIEU_DU_DRV + " TEXT, " +
                DESTINATION + " TEXT, " +
                HEUR_DU_RDV + " TEXT, " +
                NOMBRE_DE_PLACES_PROPOSEES +" TEXT)";

        //CREATE TABLE PERSON
        String createTablePerson = "CREATE TABLE " + TABLE_NAME_Person + " ("+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NOM + " TEXT, " +
                PRENOM + " TEXT, " +
                TEL + " TEXT, " +
                EMAIL + " TEXT, " +
                LOGIN + " TEXT, " +
                PASSWORD +" TEXT)";

        db.execSQL(createTable);
        db.execSQL(createTablePerson);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP  TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP  TABLE IF EXISTS " + TABLE_NAME_Person);
        onCreate(db);
    }


    // INSERTION  METHOD IN OFFRE TABLE
    public boolean addData(String item1,String item2,String item3,String item4,String item5,String item6,String item7,String item8,String item9) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NOM, item1);
        contentValues.put(PRENOM, item2);
        contentValues.put(MODELE, item3);
        contentValues.put(COULEUR, item4);
        contentValues.put(IMMATRICULATION, item5);
        contentValues.put(LIEU_DU_DRV, item6);
        contentValues.put(DESTINATION, item7);
        contentValues.put(HEUR_DU_RDV, item8);
        contentValues.put(NOMBRE_DE_PLACES_PROPOSEES, item9);

        Log.d(TAG, "addData: Adding " + item1 + " to " + TABLE_NAME);
        //TODO

        long result = db.insert(TABLE_NAME, null, contentValues);

        //if data as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }


    // INSERTION METHOD IN PERSON TABLE
    public boolean addData(String item1,String item2,String item3,String item4,String item5,String item6){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NOM, item1);
        contentValues.put(PRENOM, item2);
        contentValues.put(TEL, item3);
        contentValues.put(EMAIL, item4);
        contentValues.put(LOGIN, item5);
        contentValues.put(PASSWORD, item6);

        // Insertion
        long result = db.insert(TABLE_NAME_Person,null,contentValues);
        if(result == -1)
            return false;
        else
            return true;


    }



    // CHECK LOGIN IF NOT EXISTE IN PERSON  TABLE
    public boolean ChekLogin(String Login){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from "+TABLE_NAME_Person+" where login = ?", new String[]{Login});
        if (cursor.getCount()>0)
            return true;
        else
            return false;
    }








    // CHECK CONNECTION
   public boolean checkconnection(String item1,String item2){
      SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery("select * from "+TABLE_NAME_Person+" where login = ? and password = ?", new String[]{item1, item2});
    if (cursor.getCount()>0)
        return true;
    else
       return false;
   }




    // LISTE OFFRE
    public ArrayList ListOffre(){
        ArrayList arrayList = new ArrayList();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + TABLE_NAME,null);
        cursor.moveToFirst();

        while (cursor.isAfterLast()== false){
            int id = cursor.getInt(0);
            String dest = cursor.getString(7);
            String hrv = cursor.getString(8);
            arrayList.add(id+"-"+dest+""+hrv);
        }

        return  arrayList;


    }

/**********************************************************************/
    // LISTE OFFRE
    public List <Offre> AllOffre(){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount()>0){
            List<Offre> offreList = null;
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Offre offre = new Offre();
                    offre.setID(Integer.parseInt(cursor.getString(0)));
                    offre.setNOM(cursor.getString(0));
                    offre.setPRENOM(cursor.getString(0));
                    offre.setMODELE(cursor.getString(0));
                    offre.setCOULEUR(cursor.getString(0));
                    offre.setIMMATRICULATION(cursor.getString(0));
                    offre.setLIEU_DU_DRV(cursor.getString(0));
                    offre.setDESTINATION(cursor.getString(0));
                    offre.setHEUR_DU_RDV(cursor.getString(0));
                    offre.setNOMBRE_DE_PLACES_PROPOSEES(cursor.getString(0));


                    // Adding offre to list
                    offreList = new ArrayList<Offre>();
                    offreList.add(offre);
                } while (cursor.moveToNext());

               
                 
    }  // return offre list
            return offreList;
        }else
            return null;

    }











}
