package com.example.monapplication;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Marker userLocationMarker;
    DatabaseHelper db = new DatabaseHelper(this);
    //Circle userLocationAccuracyCircle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //mMap = googleMap;
        // Address location = null;
        //double laltitude = double location.getLatitude();

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        //LatLng latLng=new LatLng(-34, 151);
        //CameraUpdate camUpdate=CameraUpdateFactory.newLatLng(latLng);
        //mMap.moveCamera(camUpdate);
        //mMap.animateCamera(camUpdate);


        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        Geocoder geo = new Geocoder(getApplicationContext(), Locale.FRANCE);
        try {
            //LatLng latLng = new LatLng(23, 45);
            //ArrayList <Address> endroits = new ArrayList<>();
            //endroits.add((Address) geo.getFromLocationName("Place de la RÈpublique, Paris, France", 1));

            //return liste OFFRE
            //List <Offre> loff = db.AllOffre( );
            //ArrayAdapter arrayAdabter = new ArrayAdapter(this,android);

          // List <Offre> loff = db.ListOffre();
            //offre = loff.get(0);
            //offre.getCOULEUR();

            //String d = offre.getCOULEUR();
            //Toast.makeText(getApplicationContext(),""+loff.get(0)+"" , Toast.LENGTH_LONG).show();


            List<Address> endroits = geo.getFromLocationName("Place de la RÈpublique, Paris, France", 1);
            List<Address> endroits2 = geo.getFromLocationName("7-1 Rue Meslay , Paris, France", 1);
            List<Address> endroits3 = geo.getFromLocationName("41 Place de la République, 75011 Paris, France", 1);
            Address address = endroits.get(0);
            Address address2 = endroits2.get(0);
            Address address3 = endroits3.get(0);


            LatLng rendezvous = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(rendezvous).title("TAX 1 Rendez-vous n°1"));

            LatLng rendezvous2 = new LatLng(address2.getLatitude(), address2.getLongitude());
            mMap.addMarker(new MarkerOptions().position(rendezvous2).title("TAXI 2 Rendez-vous n°2"));
            LatLng rendezvous3 = new LatLng(address3.getLatitude(), address3.getLongitude());
            mMap.addMarker(new MarkerOptions().position(rendezvous3).title("TAXI 3 Rendez-vous n°3"));

            MarkerOptions markerOptions = new MarkerOptions();
            // TAXI 1
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

            markerOptions.position(latLng);
            markerOptions.title("Taxi n°3");
            markerOptions.snippet("Taxi jaune");
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.redcar));
            markerOptions.anchor((float) 0.5, (float) 0.5);
            userLocationMarker = mMap.addMarker(markerOptions);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));

            // TAXI 2
            LatLng latLng2 = new LatLng(address2.getLatitude(), address2.getLongitude());
            markerOptions.position(latLng2);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.redcar));
            markerOptions.anchor((float) 0.5, (float) 0.5);
            userLocationMarker = mMap.addMarker(markerOptions);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng2, 17));

            // TAXI 3
            LatLng latLng3 = new LatLng(address3.getLatitude(), address3.getLongitude());
            markerOptions.position(latLng3);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.redcar));
            markerOptions.anchor((float) 0.5, (float) 0.5);
            userLocationMarker = mMap.addMarker(markerOptions);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng3, 17));

            //Toast.makeText(getApplicationContext(),""+address.getLatitude()+"" , Toast.LENGTH_LONG).show();


            /*GoogleMap.OnMarkerClickListener oMcl=new
                    GoogleMap.OnMarkerClickListener() {
                        public boolean onMarkerClick(Marker marker) {
                            Log.i("MAP", "Clic on "+marker.getTitle());
                            RendezVous("23 Place de la République, 75003 Paris");
                            return false;
                        }
                    };*/

           // mMap.setOnMarkerClickListener(oMcl);




        } catch (IOException e) {
            e.printStackTrace();
        }


    }



        public void RendezVous(Address address) {


            //LatLng latLng = new LatLng(23, 45);

           // List<Address> endroits = geo.getFromLocationName("Place de la RÈpublique, Paris, France", 1);
           // Address address = endroits.get(0);
            LatLng rendezvous = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(rendezvous).title("Rendez-vous n°1"));

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(rendezvous, 17));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(rendezvous));



    }



}